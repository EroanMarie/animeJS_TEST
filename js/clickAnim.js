var corps = document.getElementById("full");

svgNS = corps.namespaceURI;

function createFullCircles(ev) {
    var el = document.createElementNS(svgNS, 'circle');
    el.setAttributeNS(null, 'cx', ev.clientX);
    el.setAttributeNS(null, 'cy', ev.clientY);
    el.setAttributeNS(null, 'r', '40');
    el.setAttributeNS(null, 'style', 'fill:#' + Math.floor(Math.random() * 16777215).toString(16));
    return el;
}

var circles = new Array(20);

corps.addEventListener("mousedown", function(event) {
    var circle = document.createElementNS(svgNS, 'circle');
    circle.setAttributeNS(null, 'cx', event.clientX);
    circle.setAttributeNS(null, 'cy', event.clientY);
    circle.setAttributeNS(null, 'r', '1');
    circle.setAttributeNS(null, 'style', 'fill: none; stroke: white; stroke-width: 1px;');
    corps.appendChild(circle);
    for (i = 0; i < circles.length; i++) {
        circles[i] = (createFullCircles(event));
        corps.appendChild(circles[i]);
    }
    anime({
        targets: circle,
        r: anime.random(80, 160),
        lineWidth: 0,
        opacity: {
            value: 0,
            easing: 'linear',
            duration: anime.random(400, 500),
        },
        duration: anime.random(1200, 1800),
        easing: 'easeOutExpo',
        offset: 0

    });
    anime({
        targets: circles,
        translateX: function() {
            return anime.random(-150, 150);
        },
        translateY: function() {
            return anime.random(-150, 150);
        },
        r: 0,
        duration: anime.random(400, 500),
        easing: 'easeOutSine'
    });
}, false);